# hydroqc-hass-addons

**The full updated project documentation can be found at [https://hydroqc.ca](https://hydroqc.ca)**

We have a discord server where you can come to discuss and find help with the project [https://discord.gg/2NrWKC7sfF](https://discord.gg/2NrWKC7sfF)

HASS addon code for Hydro Quebec API Wrapper and hydroqc2mqtt integration

Supported Arch:
![Supports aarch64 Architecture][aarch64-shield]
![Supports amd64 Architecture][amd64-shield]
![Supports armhf Architecture][armhf-shield]
![Supports armv7 Architecture][armv7-shield]
![Supports i386 Architecture][i386-shield]

Work in progress, please report any issue.

# Install

Option 1: click this button:
[![Open your Home Assistant instance and show the add add-on repository dialog with a specific repository URL pre-filled.](https://my.home-assistant.io/badges/supervisor_add_addon_repository.svg)](https://my.home-assistant.io/redirect/supervisor_add_addon_repository/?repository_url=https%3A%2F%2Fgitlab.com%2Fhydroqc%2Fhydroqc-hass-addons)

Option 2: Go in the supervisor page -> Add-on Store -> click on the vertical "..." on the top right of the page, add this repository: https://gitlab.com/hydroqc/hydroqc-hass-addons.git

Once completed, go into the hydroqc addon and click install.

Let us know if you like the project or if you have bugs, suggestions, ideas.

[aarch64-shield]: https://img.shields.io/badge/aarch64-yes-green.svg
[amd64-shield]: https://img.shields.io/badge/amd64-yes-green.svg
[armhf-shield]: https://img.shields.io/badge/armhf-yes-green.svg
[armv7-shield]: https://img.shields.io/badge/armv7-yes-green.svg
[i386-shield]: https://img.shields.io/badge/i386-yes-green.svg
