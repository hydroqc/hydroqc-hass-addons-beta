<!-- https://developers.home-assistant.io/docs/add-ons/presentation#keeping-a-changelog -->

## v0.4.10-1

- Library hydroqc2mqtt: upgraded to [`v0.4.10`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/releases/0.4.10)
    - Change: People not subscribed to Winter Credits will no longer receive the sensors [`hydroqc2mqtt#20`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/issues/20)
    - Change: new pre-heat timestamp sensor for winter credits [`hydroqc2mqtt#30`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/issues/30)
    - Upgrade dependencies
- Allow to set the new pre-heat value from addon configuration

## v0.4.9-1

- Library hydroqc2mqtt: upgraded to [`v0.4.9`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/releases/0.4.9) **Make sure to read the linked release notes**
    - Bugfix: Fix timezone for hourly consumption
    - Change: add clear history button
    - Change: add config for number of days to import history for
    - Upgrade dependencies

## v0.4.8-2
- Bugfix: Fix customer, account and contract ID validation in add-on config

## v0.4.8-1
- Library hydroqc2mqtt: upgraded to [`v0.4.8`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/tags/0.4.8)
    - Bugfix: hydroqc2mqtt log-level was always INFO, despite the log-level set in add-on configuration [`hydroqc2mqtt!89`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/merge_requests/89)
    - Change: Reduce the caching for some of the account and period values to 6h instead of 24h [`hydroqc!116`](https://gitlab.com/hydroqc/hydroqc/-/merge_requests/116)
    - Upgrade dependencies

## v0.4.7-1
- Addon change
    - Re-enable custom AppArmor profile
    - Fix fr translation
- Library hydroqc2mqtt: upgraded to [`v0.4.7`](https://gitlab.com/hydroqc/hydroqc2mqtt/-/tags/0.4.7)

## v0.4.6-2
- Addon security change
    - Disable our custom AppArmor profile [`hydroqc-hass-addons!14`](https://gitlab.com/hydroqc/hydroqc-hass-addons/-/merge_requests/14)

## v0.4.6
⚠️ WARNING: requires reinstallation of the add-on
- Addon general improvements
    - The configuration is now translated (en/fr)
    - The addon is now easier to configure (breaking change)
- Addon security improvements
    - The addon is now signed, to prevent any tampering of the official image
    - An AppArmor profile has been set
- Library hydroqc2mqtt: upgraded to v0.4.6
    - Bring minor improvements

## 0.4.5
- Library hydroqc2mqtt: upgraded to v0.4.5
    - Bring minor improvements

## 0.4.4
⚠️ WARNING: requires reinstallation of the add-on
- Pre-built add-on image for the following architectures (amd64, i386, armhf, armv7, aarch64) to allow faster installation.
- The library hydroqc2mqtt has been upgraded to v0.4.4
    - This version now includes hourly consumption compatible with HASS Energy Dashboard. See docs for configuration steps

## 0.3.1
- Add new s6 overlay v3.x compatibility: fix https://gitlab.com/hydroqc/hydroqc-hass-addons/-/issues/4

## 0.3.0
- Bump hydro2mqtt to 0.3.0, fixes the following:
    - Fix error since winter credit end (April 1st)

## 0.2.4
- Bump hydro2mqtt to 0.2.2, fixes the following:
    - Fix non wintercredit accounts
    - Improve login

## 0.2.3
- Fix issue with custom MQTT server
- The structure of the configuration have changed a bit, you may have to do a complete reinstall of the addon if you encounter any issue. Make sure to copy the "hydro_quebec:" section of your config to easily reconfigure after re-install.

## 0.2.2
- hydro2mqtt 0.2.1

**Breaking Change**
- You will have to reconfigure the extension with your Hydro-Quebec information in the new format.
- With hydro2mqtt 0.2.1 the way the sensors are named changed. You will have to update your automations and lovelace configs to use the new names.
- If you have old sensors still showing try deleting the discovery topics of the old sensors in your MQTT server. All Hydroqc sensors will be under homeassistant/sensor/{yourcontractnumber} and under /homeassistant/binary_sensor/{yourcontractnumber}. If you delete all the topics they will be recreated when you restart the addon.

## 0.2.1
- Revert hydro2mqtt to 0.1.6 until 0.2.1 fix is released

## 0.2.0

**Breaking Change**
You will have to reconfigure the extension with your Hydro-Quebec information in the new format.

**New feature**

Auto-configuration for HASSOS mqtt is now available. Unless you are using a custom MQTT server the addon will be able to autodiscover and configure your MQTT addon information.

**Changes**
- addon configuration structure (beaking)
- add documentation to addon
- add home-assistant mqtt auto-discovery

## 0.1
- Initial release

Refactoring using the new Hydro Quebec API Wrapper and hydroqc2mqtt
